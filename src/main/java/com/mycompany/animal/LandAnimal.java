/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author Aritsu
 */
public abstract class  LandAnimal extends Animal {
    
    public LandAnimal(String name,int numberOfLeg) {
        super(name,numberOfLeg);
    }
    public abstract void run();
    
}
