/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author Aritsu
 */
public class Snake extends Reptile {
    private String nickname; 
    public Snake(String nickname) {
        super("Snake", 0);
        this.nickname = nickname;
    }
    
    @Override
    public void crawl() {
        System.out.println("Snake: "+ nickname +" crawl");
    }

    @Override
    public void eat() {
         System.out.println("Snake: "+ nickname +" eat");
    }

    @Override
    public void walk() {
         System.out.println("Snake: "+ nickname +" walk");
    }

    @Override
    public void speak() {
        System.out.println("Snake: "+ nickname +" speak");
    }

    @Override
    public void sleep() {
         System.out.println("Snake: "+ nickname +" sleep");
    }
    
}
