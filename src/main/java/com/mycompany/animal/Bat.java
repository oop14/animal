/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author Aritsu
 */
public class Bat extends Poultry {
    private String nickname; 
    public Bat(String nickname) {
        super("Bat", 2);
        this.nickname = nickname;
    }
    
    @Override
    public void fly() {
        System.out.println("Bat: "+ nickname +" fly");
    }

    @Override
    public void eat() {
         System.out.println("Bat: "+ nickname +" eat");
    }

    @Override
    public void walk() {
         System.out.println("Bat: "+ nickname +" walk");
    }

    @Override
    public void speak() {
        System.out.println("Bat: "+ nickname +" speak");
    }

    @Override
    public void sleep() {
         System.out.println("Bat: "+ nickname +" sleep");
    }
    
}
