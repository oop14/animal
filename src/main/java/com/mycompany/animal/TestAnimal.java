/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author Aritsu
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        Dog d1 = new Dog("Dogy");
        d1.eat();
        d1.speak();
        d1.run();
        d1.walk();
        Cat c1 = new Cat("Caty");
        c1.eat();
        c1.speak();
        c1.run();
        c1.walk();
        c1.sleep();
        Crocodile cr1 = new Crocodile("Crody");
        cr1.eat();
        cr1.crawl();
        Snake s1 = new Snake("Snaky");
        s1.crawl();
        s1.eat();
        Fish f1 = new Fish("Fishy");
        f1.eat();
        f1.swim();
        Crab cb1 = new Crab("Craby");
        cb1.eat();
        cb1.walk();
        Bat b1 = new Bat("Baty");
        b1.fly();
        b1.eat();
        Bird bi1 = new Bird("Birdy");
        bi1.fly();
        bi1.eat();
        bi1.walk();
    }
}
